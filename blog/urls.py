from django.urls import path
from .views import landing_page, posts, post_details

urlpatterns = [
    path("", landing_page, name="landing_page"),
    path("posts", posts, name="posts"),
    path("posts/<>", post_details, name="post_details")
]